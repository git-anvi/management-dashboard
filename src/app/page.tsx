import DashboardContent from "@/components/sections/DashboardContent";
import GeneralContainer from "@/components/sections/GeneralContainer";

export default function Home() {
  return (
    <>
      <GeneralContainer>
        <DashboardContent />
      </GeneralContainer>
    </>
  )
}
