"use client"
import Image from "next/image";
import ThemeSwitch from "./ThemeSwitcher"
import { FiPieChart, FiUsers, FiMenu, FiCreditCard, FiFolder, FiBell, FiSearch, FiMessageSquare, FiSettings, FiPackage, FiSliders, FiBarChart, FiAirplay, FiArrowUpRight, FiBook } from "react-icons/fi";
import { useCallback, useEffect, useRef, useState } from "react";


const littleBarOptions = [
    { isActive: true, key: 1, icon: <FiPieChart />, redirect_link: "/"},
    { isActive: false, key: 2, icon: <FiCreditCard />, redirect_link: "/"},
    { isActive: false, key: 3, icon: <FiFolder />, redirect_link: "/"},
    { isActive: false, key: 4, icon: <FiUsers />, redirect_link: "/"},
    { isActive: false, key: 5, icon: <FiPackage />, redirect_link: "/"},
    { isActive: false, key: 6, icon: <FiSliders />, redirect_link: "/"},
    { isActive: false, key: 7, icon: <FiSettings />, redirect_link: "/"},
];

const navs = [
    {
        id: 1,
        category: "Dashboard",
        content: [
            { isActive: true, key:"c1", icon: <FiPieChart />, redirect_link: "/", label: "Overview"},
            { isActive: false, key:"c2", icon: <FiBarChart />, redirect_link: "/", label: "Reports"},
        ]
    },
    {
        id: 2,
        category: "Finance",
        content: [
            { isActive: false, key:"c3", icon: <FiCreditCard />, redirect_link: "/", label: "Financial"},
            { isActive: false, key:"c4", icon: <FiAirplay />, redirect_link: "/", label: "Analytics"},
            { isActive: false, key:"c5", icon: <FiArrowUpRight />, redirect_link: "/", label: "Expenses"},
            { isActive: false, key:"c6", icon: <FiBook />, redirect_link: "/", label: "Incentive"},
        ]
    }
]

export default function GeneralContainer({ children }: { children: React.ReactNode }) {

    const box = useRef<HTMLDivElement>(null)

    const controller_1 = useRef<HTMLDivElement>(null)
    const controller_2 = useRef<HTMLDivElement>(null)

    const [openMenu, setOpenMenu] = useState(false)

   

    const handleOutsideClick = useCallback((event: { target: any }) => {
        if (box?.current && !box?.current?.contains(event.target)) {
            if(
                (controller_1?.current && !controller_1?.current?.contains(event.target)) ||
                (controller_2?.current && !controller_2?.current?.contains(event.target))
            ) {
                setOpenMenu(false)
            }
            
        }
    }, [openMenu])

    useEffect(() => {
        document.addEventListener("click", handleOutsideClick)

        return () => {
            document.removeEventListener("click", handleOutsideClick)
        }
    }, [handleOutsideClick])

    const toggleMenu = () => {
        setOpenMenu(!openMenu)
    }

    return (
        <>
            
            {/* sidebar */}
            <section  
                ref={box} 
                className={`
                ${openMenu ? "translate-x-[350px] lg:translate-x-0 " : "translate-x-[-350px] lg:translate-x-0"}
                    h-screen w-[300px] transition-all duration-500 z-50 bg-white dark:bg-green-dark fixed left-[-350px] lg:left-0 top-0 border-r border-[#f2f2f2] dark:border-[#f2f2f217] flex
                `}
            >
                
                {/* icon bar */}
                <div className="border-r border-[#f2f2f2] dark:border-[#f2f2f217] px-3 h-full w-[60px] pb-6 shrink-0 flex flex-col justify-between">
                    <div className="w-full">
                        <div className="h-[70px] flex items-center justify-center w-full border-b border-[#f2f2f2] dark:border-[#f2f2f217]">
                            <div className="h-[34px] w-[34px] shrink-0 bg-base-green  rounded-tl-[4px] rounded-tr-[8px] rounded-bl-[8px] rounded-br-[6px] mx-auto"></div>
                        </div>
                        <ul className="mt-6 flex flex-col gap-3">
                            {
                                littleBarOptions.map((item: {
                                    key: number;
                                    icon: JSX.Element;
                                    redirect_link: string;
                                    isActive: boolean
                                }) => (
                                    <li key={item.key}>
                                        <div onClick={toggleMenu} className="cursor-pointer">
                                            <div className={`${item?.isActive ? "bg-[#e6eaeb] dark:bg-green-light text-base-green " : "sm:hover:bg-[#e6eaeb] text-base-gray dark:text-white/60 sm:hover:text-base-green dark:sm:hover:bg-green-light "} h-[30px] w-[30px] transition-all cursor-pointer mx-auto  flex items-center justify-center rounded-[4px]`}>
                                                {item?.icon}
                                            </div>
                                        </div>
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                    <ThemeSwitch />
                </div>

                {/* main sidebar content */}
                <div className="w-full">
                    <div className="border-b border-[#f2f2f2] dark:border-[#f2f2f217] h-[70px] flex items-center gap-3 px-4">
                        <div className="relative h-[40px] w-[40px] rounded-full shrink-0 overflow-hidden">
                            <Image
                                src="/assets/user.jpg"
                                alt="user"
                                className="object-cover"
                                fill
                                priority
                            />
                        </div>
                        <div className="w-full">
                            <span className="block text-[14px] leading-[20px] font-[600] ">{"Emir Jamil"}</span>
                            <span className="block text-[12px] leading-[22px] text-base-gray/50 dark:text-white/60 ">{"HR Lead"}</span>
                        </div>
                    </div>
                    <div className="px-4 py-6">
                        {
                            navs?.map((item: any) => (
                                <div key={item.id} className="mb-4">
                                    <span className="text-[12px] leading-[22px]  text-base-gray/50 dark:text-white/20 block">{item?.category}</span>
                                    <ul className="mt-2 flex flex-col gap-1">
                                        {
                                            item.content.map((el: any) => (
                                                <li key={el.key}>
                                                    <div onClick={toggleMenu} className="cursor-pointer">
                                                        <div className={`${el.isActive ? "bg-base-green/10 dark:bg-green-light" : "sm:hover:bg-base-green/10 dark:sm:hover:bg-green-light"} transition-all flex group items-center gap-2 px-3 py-[10px] rounded-[10px]`}>
                                                            <div className={`${el.isActive ? "text-base-green" : "sm:hover:bg-[#e6eaeb] text-base-gray dark:text-white/60 dark:sm:group-hover:text-base-green  "} h-[26px] w-[26px] shrink-0 flex items-center justify-center`}>
                                                                {el.icon}
                                                            </div>
                                                            <span className={`block text-[14px] leading-[20px] ${el.isActive ? "font-[600] text-base-green " : "font-[400] text-base-gray dark:text-white/60 dark:sm:group-hover:text-base-green"} `}>{el.label}</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            ))
                                        }
                                        
                                    </ul>
                                </div>
                            ))
                        }
                        
                    </div>
                </div>
            </section>
            <div className={`${openMenu ? "h-screen lg:h-0" : "h-0"} bg-base-gray/60 dark:bg-base-green/60 fixed top-0 left-0 z-40 w-full`}></div>

            {/* main content */}
            <main className="lg:pl-[300px] ">

                {/* top bar */}
                <section className="border-b border-[#f2f2f2] dark:border-[#f2f2f217] h-[70px] flex items-center justify-between px-6">
                    <div className="flex items-center gap-3">
                        <div ref={controller_1} onClick={toggleMenu} className="h-[40px] w-[40px] transition-all sm:hover:bg-base-green/5 dark:sm:hover:bg-white/5 rounded-full border border-[#f2f2f2] dark:border-[#f2f2f217] shrink-0 lg:hidden flex items-center justify-center cursor-pointer ">
                            <FiMenu className="text-base-gray dark:text-white/75" />
                        </div>
                        <div className="hidden sm:flex items-center border border-[#f2f2f2] dark:border-[#f2f2f217] rounded-full pr-4">
                            <input 
                                type="text" 
                                placeholder="Search something"
                                className="bg-transparent focus:outline-none text-[14px] leading-[24px] px-4 py-2 placeholder:text-base-gray/50 dark:placeholder:text-white/60 " 
                            />
                            <FiSearch className="text-base-gray dark:text-white/75" />
                        </div>
                    </div>
                    <div className="flex items-center gap-3">
                        <div className="sm:hidden h-[40px] w-[40px] transition-all sm:hover:bg-base-green/5 dark:sm:hover:bg-white/5 rounded-full border border-[#f2f2f2] dark:border-[#f2f2f217] shrink-0 flex items-center justify-center cursor-pointer ">
                            <FiSearch className="text-base-gray dark:text-white/75" />
                        </div>
                        <div className="h-[40px] w-[40px] transition-all sm:hover:bg-base-green/5 dark:sm:hover:bg-white/5 rounded-full border border-[#f2f2f2] dark:border-[#f2f2f217] shrink-0 flex items-center justify-center cursor-pointer ">
                            <FiMessageSquare className="text-base-gray dark:text-white/75" />
                        </div>
                        <div className="h-[40px] w-[40px] transition-all sm:hover:bg-base-green/5 dark:sm:hover:bg-white/5 rounded-full border border-[#f2f2f2] dark:border-[#f2f2f217] shrink-0 flex items-center justify-center cursor-pointer ">
                            <FiBell className="text-base-gray dark:text-white/75" />
                        </div>
                        <div ref={controller_2} onClick={toggleMenu} className="relative cursor-pointer h-[40px] w-[40px] lg:hidden block rounded-full shrink-0 overflow-hidden">
                            <Image
                                src="/assets/user.jpg"
                                alt="user"
                                className="object-cover"
                                fill
                                priority
                            />
                        </div>
                    </div>
                </section>

                <section className="px-4 sm:px-6 pt-6">
                    {children}
                </section>
            </main>
        </>
    )
}