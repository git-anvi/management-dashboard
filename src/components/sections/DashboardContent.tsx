"use client"

import { useState } from "react";
import { FiChevronDown, FiChevronRight } from "react-icons/fi";
import Image from "next/image";

type period = {
    id: number;
    zone: string;
    datas: {
        key: string;
        month: string;
        good: string;
        bad: string;
    }[];
}

const employeeHappinessGraph = [
    {
        id: 1,
        zone: "January - June",
        datas: [
            {key: "month_1", month: "Jan", good: "30%", bad: "70%"},
            {key: "month_2", month: "Feb", good: "45%", bad: "55%"},
            {key: "month_3", month: "Mar", good: "40%", bad: "60%"},
            {key: "month_4", month: "Apr", good: "30%", bad: "70%"},
            {key: "month_5", month: "May", good: "60%", bad: "40%"},
            {key: "month_6", month: "June", good: "75%", bad: "25%"},
        ]
    },
    {
        id: 2,
        zone: "July - December",
        datas: [
            {key: "month_7", month: "July", good: "80%", bad: "20%"},
            {key: "month_8", month: "Aug", good: "85%", bad: "15%"},
            {key: "month_9", month: "Sep", good: "70%", bad: "30%"},
            {key: "month_10", month: "Oct", good: "10%", bad: "90%"},
            {key: "month_11", month: "Nov", good: "80%", bad: "20%"},
            {key: "month_12", month: "Dec", good: "95%", bad: "5%"},
        ]
    }
]

const employeesList = [
    {
        id: "user1",
        photo: "/assets/user-1.jpg",
        fullname: "Drake J",
        role: "Product Designer",
        experience: "6 Years",
        achievment: "40%"
    },
    {
        id: "user2",
        photo: "/assets/user-2.jpg",
        fullname: "Drake J",
        role: "Product Designer",
        experience: "3 Years",
        achievment: "60%"
    },
    {
        id: "user3",
        photo: "/assets/user-3.jpg",
        fullname: "Alou P",
        role: "Software Engineer",
        experience: "10 Years",
        achievment: "75%"
    },
    {
        id: "user4",
        photo: "/assets/user-4.jpg",
        fullname: "Dadie F",
        role: "Product Manager",
        experience: "19 Years",
        achievment: "38%"
    },
    {
        id: "user5",
        photo: "/assets/user-5.jpg",
        fullname: "Kakou H",
        role: "Brand Designer",
        experience: "7 Years",
        achievment: "15%"
    },
]

export default function DashboardContent () {

    const [isFirstPeriodActive, setIsFirstPeriodActive] = useState(true)

    function getSelectedPeriod() {

        if(isFirstPeriodActive) {
            return employeeHappinessGraph.find((x:period) => x.id === 1)
        }

        if(!isFirstPeriodActive) {
            return employeeHappinessGraph.find((x:period) => x.id === 2)
        }
    }

    function toggleEventPeriod(val: boolean) {
        setIsFirstPeriodActive(val)
    }

    return (
        <>
            {/* card stats */}
            <section className="grid grid-cols-1 md:grid-cols-3 gap-4 mb-6">
                <Card 
                    label={"Total Employee"}
                    numb={4290}
                    updatedDate="22/11/2023" 
                    variant={"green"}
                />
                <Card 
                    label={"Total Applicants"}
                    numb={2210}
                    updatedDate="22/11/2023" 
                    variant={"blue"}
                />
                <Card 
                    label={"Projects running"}
                    numb={120}
                    updatedDate="22/11/2023" 
                    variant={"saumon"}
                />
            </section>

            {/*  graphics zone */}
            <section className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-6">

                {/* #1 */}
                <div className="border border-[#f2f2f2] dark:border-[#f2f2f217] p-4 rounded-[10px] ">
                    
                    <div className="flex items-center justify-between">
                        <span className="block text-[16px] sm:text-[18px] leading-[26px] sm:leading-[28px] font-[500] ">{"Employee happiness"}</span>
                        <div className="relative border border-[#f2f2f2] rounded-full cursor-pointer sm:hover:bg-base-green/5 dark:sm:hover:bg-white/5 transition-all dark:border-[#f2f2f217] sm:px-4 px-3 py-3 flex items-center">
                            <div className="w-[130px] shrink-0 hidden sm:block ">
                                <span className="text-[14px] leading-[24px] block font-[400] opacity-50 relative z-20">{getSelectedPeriod()?.zone}</span>
                            </div>
                            <FiChevronDown/>
                            {/* <div className="bg-white dark:bg-green-dark border z-30 border-[#f2f2f2] dark:border-[#f2f2f217] px-2 py-2 absolute top-[55px] right-0 w-[220px] shadow-sm rounded-[10px]  ">
                                <ul>
                                    <li onClick={() => toggleEventPeriod(true)} className="py-2 px-2 rounded-[10px] sm:hover:bg-green-light/50 dark:sm:hover:bg-[#f2f2f217] transition-all ">
                                        <span className="text-[14px] leading-[24px] block font-[400] opacity-50 relative z-20">
                                            {"January - June"}
                                        </span>
                                    </li>
                                    <li onClick={() => toggleEventPeriod(false)} className="py-2 px-2 rounded-[10px] sm:hover:bg-green-light/50 dark:sm:hover:bg-[#f2f2f217] transition-all ">
                                        <span className="text-[14px] leading-[24px] block font-[400] opacity-50 relative z-20">
                                            {"July - December"}
                                        </span>
                                    </li>
                                </ul>
                            </div> */}
                        </div>
                    </div>

                    <div className="flex items-start gap-1 justify-between mt-4">
                        {
                            getSelectedPeriod()?.datas.map((item: any) => (
                                <div key={item?.key} className="text-center">
                                    <div className="w-[16px] shrink-0 h-[200px] flex flex-col gap-1 mb-2 ">
                                        <div className=" rounded-[6px] bg-[#010035] w-full dark:bg-[#2196F3]  " style={{height: item?.good}}></div>
                                        <div className=" rounded-[6px] bg-[#d0d2ef]  w-full  " style={{height: item?.bad}}></div>
                                    </div>
                                    <span className="text-[12px] leading-[24px] block font-[400] opacity-50">{item?.month}</span>
                                </div>
                            ))
                        }
                    </div>

                    <div className="mt-4 flex items-center gap-6">
                        <div className="flex items-center gap-2">
                            <div className="h-[14px] w-[8px] rounded-[2px] bg-[#010035] dark:bg-[#2196F3] shrink-0 "></div>
                            <span className="text-[14px] leading-[24px] block font-[400] text-base-gray">{"Good"}</span>
                        </div>
                        <div className="flex items-center gap-2">
                            <div className="h-[14px] w-[8px] rounded-[2px] bg-[#d0d2ef] shrink-0 "></div>
                            <span className="text-[14px] leading-[24px] block font-[400] text-base-gray">{"Bad"}</span>
                        </div>
                    </div>
                </div>

                {/* #2 */}
                <div className="border border-[#f2f2f2] dark:border-[#f2f2f217] p-4 rounded-[10px] ">
                    
                    <div className="flex items-center justify-between">
                        <span className="block text-[16px] sm:text-[18px] leading-[26px] sm:leading-[28px] font-[500] ">{"Project Overview"}</span>
                        <div className="h-[50px] ">&nbsp;</div>
                    </div>

                    <div className="md:h-[230px] mt-4">

                        <div className="">
                            <div className="flex items-center gap-6">
                                <div className="flex flex-col sm:flex-row sm:items-end sm:gap-2">
                                    <span className="text-[20px] sm:text-[30px] leading-[30px] sm:leading-[40px] block font-[500] relative z-20">{"120"}</span>
                                    <span className="text-[12px] leading-[24px] block font-[400] opacity-50 capitalize relative z-20">{`Projects running `}</span>
                                </div>
                                <div className="flex flex-col sm:flex-row sm:items-end sm:gap-2">
                                    <span className="text-[20px] sm:text-[30px] leading-[30px] sm:leading-[40px] block font-[500] relative z-20">{"80%"}</span>
                                    <span className="text-[12px] leading-[24px] block font-[400] opacity-50 capitalize relative z-20">{`Overall progress `}</span>
                                </div>
                            </div>
                        </div>
                        <div className="mt-4">
                            <div className=" relative flex rounded-[10px] overflow-hidden">
                                <div className="w-[10%]">
                                    <div className="h-[50px] sm:h-[80px] w-full bg-[#5d4d33] "></div>
                                </div>
                                <div className="w-[50%]">
                                    <div className="h-[50px] sm:h-[80px] w-full bg-[#e7f3d9] "></div>
                                </div>
                                <div className="w-[40%]">
                                    <div className="h-[50px] sm:h-[80px] w-full bg-[#ecfdec] dark:bg-[#e5db81] "></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mt-4 flex flex-col sm:flex-row sm:items-center gap-2 sm:gap-6">
                        <div className="flex items-center gap-2">
                            <div className="h-[14px] w-[8px] rounded-[2px] bg-[#5d4d33] shrink-0 "></div>
                            <span className="text-[14px] leading-[24px] block font-[400] text-base-gray dark:text-white/50">{"24 Projects hold"}</span>
                        </div>
                        <div className="flex items-center gap-2">
                            <div className="h-[14px] w-[8px] rounded-[2px] bg-[#e7f3d9] shrink-0 "></div>
                            <span className="text-[14px] leading-[24px] block font-[400] text-base-gray dark:text-white/50">{"44 on progress"}</span>
                        </div>
                        <div className="flex items-center gap-2">
                            <div className="h-[14px] w-[8px] rounded-[2px] bg-[#ecfdec] dark:bg-[#e5db81] shrink-0 "></div>
                            <span className="text-[14px] leading-[24px] block font-[400] text-base-gray dark:text-white/50">{"45 successful"}</span>
                        </div>
                    </div>
                </div>
            </section>

            {/* table */}
            <section className="mb-10">
                <span className="block text-[16px] sm:text-[18px] leading-[26px] sm:leading-[28px] font-[500] mb-4 ">{"Employee List"}</span>
                <div className="border border-[#f2f2f2] dark:border-[#f2f2f217] overflow-hidden rounded-[10px] hidden sm:block  ">
                    <table className="w-full ">
                        <thead>
                            <tr className="text-left bg-[#f6f6f6] dark:bg-green-light/10 ">
                                <th className="text-[14px] leading-[22px] p-4 font-[500] text-base-gray dark:text-white">{"Name"}</th>
                                <th className="text-[14px] leading-[22px] p-4 font-[500] text-base-gray dark:text-white">{"Role"}</th>
                                <th className="text-[14px] leading-[22px] p-4 font-[500] text-base-gray dark:text-white">{"Experience"}</th>
                                <th className="text-[14px] leading-[22px] p-4 font-[500] text-base-gray dark:text-white">{"Achievment score"}</th>
                                <th className="text-[14px] leading-[22px] p-4 font-[500] text-base-gray dark:text-white">{"Action"} </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                employeesList?.map((item: {
                                    id: string;
                                    photo: string;
                                    fullname: string;
                                    role: string;
                                    experience: string;
                                    achievment: string;
                                }) => (
                                    <tr key={item.id} className="border-b border-[#f2f2f2] dark:border-[#f2f2f217]">
                                        <td className="p-4">
                                            <div className="flex items-center gap-3">
                                                <div className="relative h-[40px] w-[40px] rounded-full shrink-0 overflow-hidden">
                                                    <Image
                                                        src={item.photo}
                                                        alt="user"
                                                        className="object-cover"
                                                        fill
                                                        priority
                                                    />
                                                </div>
                                                <div className="w-full">
                                                    <span className="block text-[14px] leading-[20px] font-[600] ">{item?.fullname}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td className="p-4">
                                            <span className="block text-[14px] leading-[20px] font-[600] ">{item?.role}</span>
                                        </td>
                                        <td className="p-4">
                                            <span className="block text-[14px] leading-[20px] font-[600] ">{item?.experience}</span>
                                        </td>
                                        <td className="p-4">
                                            <div className="flex items-center gap-4">
                                                <div className="w-[150px] h-[8px] rounded-full relative overflow-hidden shrink-0 bg-base-gray/20 ">
                                                    <div className="h-full bg-base-green dark:bg-green-light rounded-full " style={{width: item?.achievment }}></div>
                                                </div>
                                                <span className="block text-[14px] leading-[20px] font-[600] ">{item?.achievment}</span>
                                            </div>
                                        </td>
                                        <td className="p-4">
                                            <button type="button" className="focus:outline-none cursor-pointer bg-[#f2f2f2] dark:bg-green-light dark:text-base-green sm:hover:bg-[#e7e7e7] dark:sm:hover:bg-green-light transition-all rounded-full px-6 py-3 ">
                                                <span className="block text-[14px] leading-[20px] font-[600] ">{"Détails"}</span>
                                            </button>
                                        </td>
                                    </tr>
                                ))
                            }
                            
                        </tbody>
                    </table>
                </div>
                <div className="border border-[#f2f2f2] dark:border-[#f2f2f217] overflow-hidden rounded-[10px]  sm:hidden  ">
                    <table className="w-full ">
                        <thead>
                            <tr className="text-left bg-[#f6f6f6] dark:bg-green-light/10 ">
                                <th className="text-[14px] leading-[22px] p-4 font-[500] text-base-gray dark:text-white">{`${employeesList.length} Utilisateurs`}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                employeesList?.map((item: {
                                    id: string;
                                    photo: string;
                                    fullname: string;
                                    role: string;
                                    experience: string;
                                    achievment: string;
                                }) => (
                                    <tr key={item.id} className="border-b border-[#f2f2f2] dark:border-[#f2f2f217]">
                                        <td className="p-4 flex items-center justify-between">
                                            <div className="flex items-center gap-3">
                                                <div className="relative h-[40px] w-[40px] rounded-full shrink-0 overflow-hidden">
                                                    <Image
                                                        src={item.photo}
                                                        alt="user"
                                                        className="object-cover"
                                                        fill
                                                        priority
                                                    />
                                                </div>
                                                <div className="w-full">
                                                    <span className="block text-[14px] leading-[20px] font-[600] ">{item?.fullname}</span>
                                                    <span className="block text-[12px] leading-[20px] font-[400] opacity-50 ">{item?.role}</span>
                                                </div>
                                            </div>
                                            <FiChevronRight />
                                        </td>
                                        
                                    </tr>
                                ))
                            }
                            
                        </tbody>
                    </table>
                </div>
            </section>

            <section className="p-4 flex flex-col gap-1 ">
                <a
                    className="w-full text-center"
                    href="https://instagram.com/kaydonald.digital?igshid=OGQ5ZDc2ODk2ZA=="
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <span className="block text-[12px] leading-[20px] font-[400] ">
                        <span className="opacity-50">{"Developped with love "}</span>
                        <span className="opacity-100 underline underline-offset-2">{" by Kay Donald"}</span>
                    </span>
                </a>
                <a
                    className="w-full flex items-center justify-center gap-2"
                    href="https://dribbble.com/shots/23254074-Simplus-HR-Management-Dashboard"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <span className="block text-[12px] leading-[22px] font-[400] opacity-50 text-center ">{"Inspired by a design picked on Dribble . Consultez ici"}</span>
                </a>
            </section>
        </>
    )
}

export const Card = (
    {label, numb, updatedDate, variant} : {label: string, numb: number, updatedDate: string, variant: string}
) => {
    return (
        <div className={`
            ${variant === "green" && "bg-base-green dark:bg-[#009688] text-white"} 
            ${variant === "blue" && "bg-[#d0eff0] dark:bg-[#00BCD4] text-green-dark"} 
            ${variant === "saumon" && "bg-[#fff4ea] dark:bg-[#fb9a7c] text-green-dark"} 
            border border-[#f2f2f2] dark:border-[#f2f2f217]
            h-[170px] overflow-hidden rounded-[10px] relative py-7 px-5 flex flex-col justify-between`
        }>
            <span className="text-[16px] leading-[24px] block font-[500] capitalize relative z-20 ">{label}</span>
            <div>
                <span className="text-[40px] leading-[50px] block font-[500] relative z-20">{numb}</span>
                <span className="text-[12px] leading-[24px] block font-[400] opacity-50 capitalize relative z-20">{`Updated: ${updatedDate} `}</span>
            </div>
           

            <div className={`
                ${variant === "green" && "bg-green-light/10"}
                ${variant === "blue" && "bg-white/20"}
                ${variant === "saumon" && "bg-white/30"}
                absolute h-[80px] w-[100px] bottom-0 right-[-8px] rounded-bl-[5px] rounded-tl-[20px] rounded-tr-[20px]
            `}>
                <div className="flex items-center justify-between px-4 pt-8">
                    <div className={`
                        ${variant === "green" && "bg-green-dark/20"}
                        ${variant === "blue" && "bg-[#c5e5e6bd] dark:bg-[#00BCD4]"}
                        ${variant === "saumon" && "bg-[#f8ede2b9] dark:bg-[#fb9a7c]"}
                        h-[20px] w-[20px] rounded-full shrink-0
                    `}></div>
                    <div className={`
                        ${variant === "green" && "bg-green-dark/20"}
                        ${variant === "blue" && "bg-[#c5e5e6bd] dark:bg-[#00BCD4]"}
                        ${variant === "saumon" && "bg-[#f8ede2b9] dark:bg-[#fb9a7c]"}
                        h-[20px] w-[20px] rounded-full shrink-0
                    `}></div>
                </div>
                <div className={`
                    ${variant === "green" && "bg-green-dark/20"}
                    ${variant === "blue" && "bg-[#c5e5e6bd] dark:bg-[#00BCD4]"}
                    ${variant === "saumon" && "bg-[#f8ede2b9] dark:bg-[#fb9a7c]"}
                    w-[30px] h-[14px] rounded-[4px] mx-auto mt-3
                `}></div>
            </div>
        </div>
    )
}