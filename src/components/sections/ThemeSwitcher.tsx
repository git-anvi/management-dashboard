'use client'

import { useState, useEffect } from 'react'
import { useTheme } from 'next-themes'
import { FiMoon, FiSun } from "react-icons/fi";

export default function ThemeSwitch() {
  const [mounted, setMounted] = useState(false)
  const { setTheme, resolvedTheme } = useTheme()

  useEffect(() =>  setMounted(true), [])

  if (!mounted) return (
    <div className='w-[30px] h-[59px] bg-[#e6eaeb] dark:bg-[#e6eaeb30] rounded-full mx-auto relative flex flex-col items-center justify-around'>
      &nbsp;
    </div>
  )

  return (
    <>
      <div className='w-[30px] h-[59px] bg-[#e6eaeb] dark:bg-[#e6eaeb30] rounded-full mx-auto relative flex flex-col items-center justify-around'>
        <div onClick={() => setTheme('light')} className='w-[24px] h-[24px] relative z-20 rounded-full cursor-pointer flex items-center justify-center '>
          <FiSun className={`${resolvedTheme === 'light' ? "text-white " : "dark:text-white/50 dark:sm:hover:text-white"} transition-all`} />
        </div>
        <div onClick={() => setTheme('dark')} className='w-[24px] h-[24px] relative z-20 rounded-full cursor-pointer flex items-center justify-center '>
          <FiMoon className="text-base-green dark:text-white" />
        </div>
        <div className={`${resolvedTheme === 'light' ? "" : "translate-y-[30px]" } top-[3px] w-[24px] h-[24px] rounded-full absolute  left-1/2 -translate-x-1/2 transition-all duration-300 bg-base-green `}></div>
      </div>
    </>
  )
}