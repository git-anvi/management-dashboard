import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  darkMode: ["class"],
  theme: {
    extend: {
      backgroundImage: {},
      colors: {
        "base-green": "#002f34",
        "green-dark": "#19242a",
        "green-light": "#daf2f2",
        "base-gray": "#696969"
      }
    },
  },
  plugins: [],
}
export default config
